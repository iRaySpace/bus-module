// Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
// For license information, please see license.txt

frappe.ui.form.on('Announcement', {
	refresh: function(frm) {
        if (frm.doc.__islocal) {
            frm.set_value('posted_on', frappe.datetime.now_datetime());
        }
    }
});
