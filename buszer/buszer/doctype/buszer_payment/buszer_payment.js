// Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
// For license information, please see license.txt

frappe.ui.form.on('Buszer Payment', {
	refresh: function(frm) {
		init_form(frm);
    },
	account: function(frm) {
		frappe.call({
			method: 'frappe.client.get',
			args: {
				doctype: 'Buszer Account',
				name: frm.doc.account
			},
			callback: function(r) {
				var data = r.message;
				console.log(data);
				frm.set_value('customer_name', data.first_name + ' ' + data.last_name);
			}
		})
	},
	bus_route: function(frm) {
		frappe.call({
			method: 'frappe.client.get',
			args: {
				doctype: 'Bus Route',
				name: frm.doc.bus_route
			},
			callback: function(r) {
				var data = r.message;
				frm.set_value('bus_id', data.bus);
				frm.set_value('route_from', data.from_city_municipality);
				frm.set_value('route_to', data.to_city_municipality);
				frm.set_value('fare', data.fare);
			}
		})
	}
});

var init_form = function(frm) {
	if (frm.doc.__islocal) {
        frm.set_value('posting_on', frappe.datetime.now_datetime());
    }
};