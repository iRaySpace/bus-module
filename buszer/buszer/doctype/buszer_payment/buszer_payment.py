# -*- coding: utf-8 -*-
# Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class BuszerPayment(Document):
	def validate(self):
		expected_pay = self.fare * self.pax
		if expected_pay > self.paid:
			frappe.throw(_('Paid amount should be greater than or equal to the expected pay'))

	def set_default_values(self):
		"""Set the customer name as the title"""
		if self.flags.in_insert:
			self.title = self.customer_name

	def before_save(self):
		"""Initialize the document"""
		self.set_default_values()