// Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
// For license information, please see license.txt

frappe.ui.form.on('Bus Route', {
	bus: function(frm) {
		frappe.call({
			method: 'frappe.client.get',
			args: {
				doctype: 'Bus Vehicle',
				name: frm.doc.bus
			},
			callback: function(r) {
				var data = r.message;
				frm.set_value('bus_liner', data.bus_liner);
			}
		})
	}
});
