# -*- coding: utf-8 -*-
# Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _


class BusRoute(Document):
	def validate(self):
		"""Same from and to is not allowed"""
		if self.from_city_municipality == self.to_city_municipality:
			frappe.throw(_('Same *From* and *To* is not allowed'))

	def set_default_values(self):
		"""Create the seats"""
		if self.flags.in_insert:
			"""Get bus information and add bus seats"""
			bus = frappe.get_doc('Bus Vehicle', self.bus)

			for i in range(1, bus.max_seats + 1):
				self.append('bus_seats', {
					'seat_number': i,
					'is_taken': 0
				})

			self.title = '{0}/{1}'.format(self.from_city_municipality, self.to_city_municipality)

	def before_save(self):
		"""Initialize the bus route"""
		self.set_default_values()