// Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
// For license information, please see license.txt

frappe.ui.form.on('Bus Vehicle', {
	refresh: function(frm) {
		// Create custom button
		init_form(frm);
	}
});

var init_form = function(frm) {
	if (!frm.doc.__islocal) {
        frm.add_custom_button(__('Create Bus Route'), function () {
            frappe.route_options = {
                bus: frm.doc.name
            };
            frappe.new_doc('Bus Route');
        });
    }
};