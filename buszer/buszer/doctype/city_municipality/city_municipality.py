# -*- coding: utf-8 -*-
# Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class CityMunicipality(Document):
	def set_default_values(self):
		"""Set the City/Municipality name as the title"""
		if self.flags.in_insert:
			self.title = self.city_municipality_name

	def before_save(self):
		"""Initialize the document"""
		self.set_default_values()