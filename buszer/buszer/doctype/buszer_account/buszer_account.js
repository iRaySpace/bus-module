// Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
// For license information, please see license.txt

frappe.ui.form.on('Buszer Account', {
	account: function(frm) {
		frappe.call({
			method: 'frappe.client.get',
			args: {
				doctype: 'User',
				name: frm.doc.account
			},
			callback: function(r) {
				var data = r.message;
				frm.set_value('first_name', data.first_name);
				frm.set_value('middle_name', data.middle_name);
				frm.set_value('last_name', data.last_name);
			}
		})
	}
});