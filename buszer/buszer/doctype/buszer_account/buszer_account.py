# -*- coding: utf-8 -*-
# Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class BuszerAccount(Document):
	def set_default_values(self):
		"""Set the default value for new documents"""
		if self.flags.in_insert:
			if not self.buszer_points:
				self.buszer_points = 0
			self.last_transaction_on = frappe.utils.now()

	def before_save(self):
		"""Initialize the new account"""
		self.set_default_values()
