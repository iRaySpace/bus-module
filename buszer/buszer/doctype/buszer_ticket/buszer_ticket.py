# -*- coding: utf-8 -*-
# Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _


class BuszerTicket(Document):
	def set_default_values(self):
		"""Set the document title"""
		if self.flags.in_insert:
			self.title = '{0}/{1}'.format(self.bus_liner, self.bus_route)

	def set_missing_information(self):
		"""Set missing information"""
		if self.flags.in_insert:
			payment = frappe.get_doc('Buszer Payment', self.reference_payment)
			bus_route = frappe.get_doc('Bus Route', self.bus_route)

			# Set the missing values
			self.posting_on = frappe.utils.now()
			self.bus_liner = bus_route.bus_liner
			self.departure_date = bus_route.departure_date
			self.departure_time = bus_route.departure_time
			self.destination_route = '{0}/{1}'.format(bus_route.from_city_municipality, bus_route.to_city_municipality)
			self.account = payment.account
			self.customer = payment.customer_name

	def before_save(self):
		"""Initialize the ticket"""
		self.set_missing_information()
		self.set_default_values()

	def on_submit(self):
		"""Set the bus route"""
		bus_route = frappe.get_doc('Bus Route', self.bus_route)

		# Get the bus seat
		bus_seat = bus_route.bus_seats[self.seat_number - 1]

		# If taken throw an error
		if bus_seat.is_taken:
			frappe.throw(_('Bus seat is already taken.'))

		# Set as taken
		bus_seat.is_taken = 1

		# Update bus route
		bus_route.save(ignore_permissions=True)

	def on_cancel(self):
		"""Set the bus route"""
		bus_route = frappe.get_doc('Bus Route', self.bus_route)

		# Get the bus seat
		bus_seat = bus_route.bus_seats[self.seat_number - 1]

		# Set as not taken
		bus_seat.is_taken = 0

		# Update bus route
		bus_route.save(ignore_permissions=True)
