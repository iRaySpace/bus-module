// Copyright (c) 2018, Ivan Ray Altomera, Raphael Erick Marcial and contributors
// For license information, please see license.txt

frappe.ui.form.on('Buszer Ticket', {
	reference_payment: function(frm) {
		frappe.call({
			method: 'frappe.client.get',
			args: {
				doctype: 'Buszer Payment',
				name: frm.doc.reference_payment
			},
			callback: function(r) {
				var data = r.message;
				frm.set_value('posting_on', data.posting_on);
			}
		});
	},
	bus_route: function(frm) {
		frappe.call({
			method: 'frappe.client.get',
			args: {
				doctype: 'Bus Route',
				name: frm.doc.bus_route
			},
			callback: function(r) {
				var data = r.message;
				frm.set_value('bus_liner', data.bus_liner);
				frm.set_value('destination_route', data.title);
				frm.set_value('departure_date', data.departure_date);
				frm.set_value('departure_time', data.departure_time);
			}
		})
	}
});
