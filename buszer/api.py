import json
import frappe


@frappe.whitelist(allow_guest=True)
def find_buses(data):
    """Find buses with the schedule"""
    data = json.loads(data)

    filters = {
        'from_city_municipality': data['from'],
        'to_city_municipality': data['to'],
        'departure_time': data['time']
    }

    routes = frappe.get_all('Bus Route', filters=filters)

    buses = []

    for datum in routes:
        doc = frappe.get_doc('Bus Route', datum.name)
        buses.append({
            'route_id': datum.name,
            'bus_id': doc.bus,
            'bus_liner': doc.bus_liner,
            'departure_time': doc.departure_time,
            'fare': doc.fare,
            'from': doc.from_city_municipality,
            'to': doc.to_city_municipality
        })

    return buses

@frappe.whitelist(allow_guest=True)
def get_buses():
    """Retrieve buses from the database"""
    data = frappe.get_all('Bus Route')

    # List of buses available
    buses = []

    for datum in data:
        doc = frappe.get_doc('Bus Route', datum.name)
        buses.append({
            'route_id': datum.name,
            'bus_id': doc.bus,
            'bus_liner': doc.bus_liner,
            'departure_time': doc.departure_time,
            'fare': doc.fare,
            'from': doc.from_city_municipality,
            'to': doc.to_city_municipality
        })

    return buses


@frappe.whitelist(allow_guest=True)
def get_announcements():
    """Retrieve news from the database"""
    data = frappe.get_all('Announcement')

    news = []

    for datum in data:
        doc = frappe.get_doc('Announcement', datum.name)
        news.append({
            'title': doc.title,
            'content': doc.content,
            'posted': doc.posted_on.strftime('%B %d, %Y')
        })

    return news


@frappe.whitelist(allow_guest=True)
def authenticate_user(data):
    """Checks the user and authenticate by sending back the salted email"""
    from frappe.utils.password import check_password

    data = json.loads(data)

    # For verification only if the user exists
    user = frappe.get_doc('User', data['email'])

    # Check the password
    check_password(data['email'], data['password'])

    # Initial data
    account = frappe.get_doc('Buszer Account', data['email'])

    # Data
    data = {
        "first_name": account.first_name,
        "last_name": account.last_name,
        "buszer_points": account.buszer_points,
        "key": "bU5Z3r{0}".format(data['email'])
    }

    return {"success": 1, "data": data}


@frappe.whitelist(allow_guest=True)
def register_user(data):
    """Register new user account for the application"""
    data = json.loads(data)

    new_user = frappe.get_doc({
        'doctype': 'User',
        'email': data['email'],
        'first_name': data['first_name'],
        'last_name': data['last_name'],
        'new_password': data['password']
    })

    new_user.insert(ignore_permissions=True)

    # Role for transacting in Buszer
    new_user.add_roles('Purchase User')
    new_user.save()

    # Add to the Buszer
    buszer_account = frappe.get_doc({
        'doctype': 'Buszer Account',
        'account': data['email'],
        'first_name': data['first_name'],
        'last_name': data['last_name'],
        'buszer_points': 100  # Free for all
    })

    buszer_account.insert(ignore_permissions=True)

    return {"success": 1}


@frappe.whitelist(allow_guest=True)
def book_bus(data):
    """Book the bus for you"""
    data = json.loads(data)

    # Get the fare
    bus_route = frappe.get_doc('Bus Route', data['route_id'])

    # Buszer Payment
    buszer_payment = frappe.get_doc({
        'doctype': 'Buszer Payment',
        'account': data['key'][6:],
        'customer_name': data['customer_name'],
        'posting_on': frappe.utils.now(),
        'bus_route': data['route_id'],
        'bus_id': data['bus_id'],
        'route_from': data['route_from'],
        'route_to': data['route_to'],
        'fare': bus_route.fare,
        'pax': data['qty'],
        'paid': bus_route.fare * data['qty']
    })

    buszer_payment.insert(ignore_permissions=True)

    data = {
        "name": buszer_payment.name
    }

    return {"success": 1, "data": data}


@frappe.whitelist(allow_guest=True)
def get_payment_details(data):
    """Get Payment Details"""
    data = json.loads(data)

    # Buszer Payment
    buszer_payment = frappe.get_doc('Buszer Payment', data['name'])

    data = {
        'name': buszer_payment.name,
        'customer': buszer_payment.customer_name,
        'method': buszer_payment.payment_method,
        'datetime': buszer_payment.posting_on,
        'bus_route': buszer_payment.bus_route,
        'bus_id': buszer_payment.bus_id,
        'route_from': buszer_payment.route_from,
        'route_to': buszer_payment.route_to,
        'fare': buszer_payment.fare,
        'pax': buszer_payment.pax,
        'paid': buszer_payment.paid
    }

    return {'success': 1, 'data': data}


@frappe.whitelist(allow_guest=True)
def get_ticket(data):
    """Get Ticket Details"""
    data = json.loads(data)

    # Data
    tickets = []

    for seat in data['seats']:
        seat = get_seat_number(seat)

        buszer_ticket = frappe.get_doc({
            'doctype': 'Buszer Ticket',
            'reference_payment': data['reference_payment'],
            'seat_number': seat,
            'bus_route': data['bus_route']
        })

        buszer_ticket.insert(ignore_permissions=True)

        # For reservation
        tickets.append(buszer_ticket.name)

    return {'success': 1, 'tickets': tickets}


@frappe.whitelist(allow_guest=True)
def reserve_ticket(data):
    """Reserve the tickets"""
    data = json.loads(data)

    buszer_payment = frappe.get_doc('Buszer Payment', data['reference_payment'])
    buszer_payment.flags.ignore_permissions = True
    buszer_payment.submit()

    for ticket in data['tickets']:
        buszer_ticket = frappe.get_doc('Buszer Ticket', ticket)
        buszer_ticket.flags.ignore_permissions = True
        buszer_ticket.submit()

    return {'success': 1}


@frappe.whitelist(allow_guest=True)
def get_places():
    """Get places"""
    city_municipality = frappe.get_all('City Municipality')

    places = []

    for place in city_municipality:
        places.append(place.name)

    return {'success': 1, 'data': places}


@frappe.whitelist(allow_guest=True)
def cancel_ticket(data):
    """Cancel the given ticket"""
    data = json.loads(data)

    ticket = frappe.get_doc('Buszer Ticket', data['ticket'])

    # Cancel the ticket
    ticket.flags.ignore_permissions = True
    ticket.cancel()

    return {'success': 1}


@frappe.whitelist(allow_guest=True)
def get_tickets(data):
    """Get tickets according to account"""
    data = json.loads(data)

    tickets = frappe.get_all('Buszer Ticket', filters={'account': data['key'][6:], 'docstatus': 1})

    reserved = []

    for ticket in tickets:
        ticket = frappe.get_doc('Buszer Ticket', ticket)
        route = frappe.get_doc('Bus Route', ticket.bus_route)

        reserved.append({
            'ticket_number': ticket.name,
            'ticket_price': route.fare,
            'seat_number': ticket.seat_number,
            'bus_liner': ticket.bus_liner,
            'bus_id': route.bus,
            'destination_route': ticket.destination_route,
            'departure_date': ticket.departure_date,
            'departure_time': ticket.departure_time
        })

    return {'success': 1, 'data': reserved}


@frappe.whitelist(allow_guest=True)
def get_seats_taken(data):
    """Get seats taken according to the bus route"""
    data = json.loads(data)

    bus_route = frappe.get_doc('Bus Route', data['bus_route'])

    taken = []

    # Taken Seats
    for seat in bus_route.bus_seats:
        if seat.is_taken:
            taken.append(get_lettered_seat(seat.seat_number))

    return {'success': 1, 'data': taken}


def get_lettered_seat(seat_number):
    """Get lettered seat based from seat number"""
    # Letters
    letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    # Getting the row
    row = (seat_number - 1) / 4

    # Getting the column
    column = seat_number % 4

    if column == 0:
        column = 4

    return letter[row] + str(column)


def get_seat_number(lettered_seat):
    """Get seat number from lettered seat"""
    number = {
        'A': 0,
        'B': 4,
        'C': 8,
        'D': 12,
        'E': 16,
        'F': 20,
        'G': 24,
        'H': 28,
        'I': 32,
        'J': 36
    }

    lettered_seat = list(lettered_seat)
    return number[lettered_seat[0]] + int(lettered_seat[1])
